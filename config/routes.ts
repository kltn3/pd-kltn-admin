﻿export default [
  {
    path: '/',
    component: '../layouts/BlankLayout',
    routes: [
      {
        path: '/user',
        component: '../layouts/UserLayout',
        routes: [
          {
            path: '/user/login',
            name: 'login',
            component: './user/login',
          },
          {
            path: '/user',
            redirect: '/user/login',
          },
          {
            name: 'register-result',
            icon: 'smile',
            path: '/user/register-result',
            component: './user/register-result',
          },
          {
            name: 'register',
            icon: 'smile',
            path: '/user/register',
            component: './user/register',
          },
          {
            component: '404',
          },
        ],
      },
      {
        path: '/',
        wrappers: ['@/layouts/SecurityLayout'],
        component: '@/layouts/BasicLayout',
        hideInMenu: true,
        routes: [
          {
            path: '/',
            redirect: '/config/company-type',
          },
          {
            name: 'company-type',
            path: '/config/company-type',
            component: './configuration/company-type',
          },
          {
            name: 'accounting-number',
            path: '/config/account-number',
            routes: [
              {
                path: '/config/account-number',
                redirect: '/config/account-number/list',
              },
              {
                hideInMenu: true,
                path: '/config/account-number/list',
                component: './configuration/account-number/list',
              },
              {
                hideInMenu: true,
                path: '/config/account-number/create',
                component: './configuration/account-number/form',
              },
              {
                hideInMenu: true,
                path: '/config/account-number/edit/:circularsId',
                component: './configuration/account-number/form',
              },
            ],
          },
          {
            name: 'tax',
            path: '/config/tax',
            component: './configuration/tax',
          },
          {
            path: '/profile',
            name: 'profile',
            icon: 'profile',
            hideInMenu: true,
            routes: [
              {
                path: '/',
                redirect: '/profile/basic',
              },
              {
                name: 'basic',
                icon: 'smile',
                path: '/profile/basic',
                component: './profile/basic',
              },
              {
                name: 'advanced',
                icon: 'smile',
                path: '/profile/advanced',
                component: './profile/advanced',
              },
            ],
          },
          {
            name: 'result',
            icon: 'CheckCircleOutlined',
            hideInMenu: true,
            path: '/result',
            routes: [
              {
                path: '/',
                redirect: '/result/success',
              },
              {
                name: 'success',
                icon: 'smile',
                path: '/result/success',
                component: './result/success',
              },
              {
                name: 'fail',
                icon: 'smile',
                path: '/result/fail',
                component: './result/fail',
              },
            ],
          },
          {
            name: 'exception',
            icon: 'warning',
            path: '/exception',
            hideInMenu: true,
            routes: [
              {
                path: '/',
                redirect: '/exception/403',
              },
              {
                name: '403',
                icon: 'smile',
                path: '/exception/403',
                component: './exception/403',
              },
              {
                name: '404',
                icon: 'smile',
                path: '/exception/404',
                component: './exception/404',
              },
              {
                name: '500',
                icon: 'smile',
                path: '/exception/500',
                component: './exception/500',
              },
            ],
          },
          {
            name: 'account',
            icon: 'user',
            path: '/account',
            hideInMenu: true,
            routes: [
              {
                path: '/',
                redirect: '/account/center',
              },
              {
                name: 'center',
                icon: 'smile',
                path: '/account/center',
                component: './account/center',
              },
              {
                name: 'settings',
                icon: 'smile',
                path: '/account/settings',
                component: './account/settings',
              },
            ],
          },
          {
            name: 'editor',
            icon: 'highlight',
            path: '/editor',
            hideInMenu: true,
            routes: [
              {
                path: '/',
                redirect: '/editor/flow',
              },
              {
                name: 'flow',
                icon: 'smile',
                path: '/editor/flow',
                component: './editor/flow',
              },
              {
                name: 'mind',
                icon: 'smile',
                path: '/editor/mind',
                component: './editor/mind',
              },
              {
                name: 'koni',
                icon: 'smile',
                path: '/editor/koni',
                component: './editor/koni',
              },
            ],
          },
          {
            component: '404',
          },
        ],
      },
    ],
  },
];
