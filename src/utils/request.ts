/** Request https://github.com/umijs/umi-request */
import { notification } from 'antd';
import { extend } from 'umi-request';

const codeMessage: Record<number, string> = {
  200: 'Thành công',
  201: 'Dữ liệu mới hoặc đã sửa đổi thành công.',
  202: 'Một request đã vào hàng đợi nền (tác vụ không đồng bộ).',
  204: 'Dữ liệu đã được xóa thành công.',
  400: 'Đã xảy ra lỗi trong request được gửi và máy chủ không tạo hoặc sửa đổi dữ liệu.',
  401: 'Người dùng không có quyền (mã thông báo, tên người dùng, mật khẩu bị sai).',
  403: 'Người dùng được ủy quyền, nhưng quyền truy cập bị cấm.',
  404: 'Request được gửi không tồn tại hoặc máy chủ không hoạt động.',
  406: 'Định dạng của request không có sẵn.',
  410: 'Tài nguyên được request sẽ bị xóa vĩnh viễn và sẽ không còn nữa.',
  422: 'Khi tạo một đối tượng, một lỗi xác thực đã xảy ra.',
  500: 'Đã xảy ra lỗi trong máy chủ, vui lòng kiểm tra máy chủ.',
  502: 'Máy chủ đã xảy ra lỗi',
  503: 'Dịch vụ không khả dụng và máy chủ tạm thời bị quá tải hoặc được bảo trì.',
  504: 'Timeout - Request đã hết hạn.',
};

/** Handler Error From Server */
const errorHandler = (error: { response: Response }): Response => {
  const { response } = error;
  if (response && response.status) {
    const errorText = codeMessage[response.status] || response.statusText;
    const { status, url } = response;

    notification.error({
      message: `Yêu cầu lỗi ${status}: ${url}`,
      description: errorText,
    });
  } else if (!response) {
    notification.error({
      description: 'Mạng của bạn không bình thường và bạn không thể kết nối với máy chủ',
      message: 'Mạng bất thường',
    });
  }

  return response;
};
/** Request Config */
const request = extend({
  // credentials: 'include', // Có thêm cookie hay không
  errorHandler, // Handle lỗi
});

request.interceptors.request.use(
  (url, options) => {
    const accessToken: string | null = localStorage.getItem('pd-authority');

    const headers = {
      'Access-Control-Allow-Origin': '*',
      Authorization: accessToken ? `Bearer ${accessToken}` : '',
    };
    return {
      url: `${API_BASE_URL}${url}`,
      options: { ...options, headers },
    };
  },
  { global: true },
);

export default request;
