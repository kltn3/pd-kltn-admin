import { useIntl } from 'umi';

export const getLocaleMessage = ({
  id,
  defaultMessage,
  values,
}: {
  id: string;
  defaultMessage: string;
  values?: any;
}) => {
  const intl = useIntl();
  return intl.formatMessage({ id, defaultMessage }, { ...values });
};
