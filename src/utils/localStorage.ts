export const getLocalStorage = (key: string) => {
  console.log(key);
  try {
    return localStorage.getItem(key);
  } catch {
    // if the item does'n exist, return null
    return null;
  }
};

export const setLocalStorage = (key: string, value: string): null | void => {
  if (!key || !value) {
    return null;
  }
  localStorage.setItem(key, value);
};
