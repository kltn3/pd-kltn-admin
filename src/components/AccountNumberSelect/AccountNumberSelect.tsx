/* eslint-disable react-hooks/exhaustive-deps */
import React from 'react';
import { TreeSelect } from 'antd';
import { Spin } from 'antd';
import type { TreeSelectProps } from 'antd';
import type { Dispatch } from 'umi';
import { connect } from 'umi';
import type { ConnectState } from '@/models/connect';
import { TreeNode } from 'rc-tree-select';

interface AccountNumberProps {
  dispatch: Dispatch;
  accountNumberRecords: API.AccountNumberRecord[];
  onChange: (value: string[]) => void;
  value: string;
  loading?: boolean;
  treeSelectProps?: TreeSelectProps<any>;
}

const AccountNumberSelect: React.FC<AccountNumberProps> = (props) => {
  const { dispatch, accountNumberRecords, onChange, value, loading, treeSelectProps } = props;
  const [selectedAccountNumber, setSelectedAccountNumber] = React.useState<string>();
  const [accountNumbers, setAccountNumbers] = React.useState<API.AccountNumber[]>([]);
  const isEmptyAccountNumberRecords = accountNumberRecords.length < 1;

  React.useEffect(() => {
    setSelectedAccountNumber(value);
  }, [value]);

  React.useEffect(() => {
    if (isEmptyAccountNumberRecords) {
      dispatch({
        type: 'configuration/fetchAccountNumber',
      });
    }
  }, [isEmptyAccountNumberRecords]);

  React.useEffect(() => {
    if (!isEmptyAccountNumberRecords) {
      setAccountNumbers(accountNumberRecords[1].accountNumbers);
    }
  }, [accountNumberRecords]);

  const passingOnChange = React.useCallback(
    (selectedValue: any) => {
      onChange(selectedValue);
    },
    [value],
  );

  const handleOnChange = (value: any, _: any) => {
    setSelectedAccountNumber(value);
    passingOnChange(value);
  };

  const renderTreeNode = (data: API.AccountNumber[]) => {
    return data.map((record) => {
      return (
        <TreeNode
          key={record.accountNumber?.toString()}
          value={record.accountNumber?.toString() as string}
          title={`${record.accountNumber} - ${record.accountName}`}
          selectable={!record.children}
        >
          {record.children && renderTreeNode(record.children as API.AccountNumber[])}
        </TreeNode>
      );
    });
  };

  return (
    <Spin spinning={loading}>
      <TreeSelect
        {...treeSelectProps}
        showSearch
        style={{ width: '100%' }}
        value={selectedAccountNumber}
        dropdownStyle={{ maxHeight: 400, overflow: 'auto' }}
        allowClear
        treeDefaultExpandAll
        onChange={handleOnChange}
        placeholder="Nhập số tài khoản để tìm kiếm"
      >
        {renderTreeNode(accountNumbers)}
      </TreeSelect>
    </Spin>
  );
};

export default connect(({ loading, configuration }: ConnectState) => ({
  accountNumberRecords: configuration.accountNumberRecords,
  loading: loading.effects['configuration/fetchAccountNumber'],
}))(AccountNumberSelect);
