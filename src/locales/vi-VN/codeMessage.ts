export default {
  'request.codeMessage.200': 'Thành công',
  'request.codeMessage.201': 'Dữ liệu mới hoặc đã sửa đổi thành công.',
  'request.codeMessage.202': 'Một yêu cầu đã vào hàng đợi nền (tác vụ không đồng bộ).',
};
