export default {
  'app.pwa.offline': 'Bạn đang offline',
  'app.pwa.serviceworker.updated': 'New content is available',
  'app.pwa.serviceworker.updated.hint': 'Please press the "Refresh" button to reload current page',
  'app.pwa.serviceworker.updated.ok': 'Làm mới',
};
