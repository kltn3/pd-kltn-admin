export default {
  'pages.account-number.list': 'Danh sách tài khoản kế toán',
  'pages.account-number.create': 'Thêm tài khoản',
  'pages.account-number.column.circular': 'Thông tư',
  'pages.account-number.column.actions': 'Hành động',
  'pages.account-number.form.account-list-level': 'Danh sách tài khoản cấp {level}',
  'pages.account-number.form.account-name': 'Tên tài khoản cấp {level}',
  'pages.account-number.form.account-number': 'Số tài khoản cấp {level}',
  'pages.account-number.form.add-account': 'Thêm tài khoản cấp {level}',
  'pages.account-number.success': '{title} tài khoản thành công',
};
