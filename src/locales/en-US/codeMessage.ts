export default {
  'request.codeMessage.200': 'Success',
  'request.codeMessage.201': 'Data is updated successful.',
  'request.codeMessage.202': 'Request is in stack.',
};
