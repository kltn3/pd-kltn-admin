export default {
  'pages.account-number.list': 'Account Number List',
  'pages.account-number.create': 'Create Account Number',
  'pages.account-number.column.circular': 'Circular',
  'pages.account-number.column.actions': 'Actions',
  'pages.account-number.form.account-list-level': 'Account list level {level}',
  'pages.account-number.form.account-name': 'Account name level {level}',
  'pages.account-number.form.account-number': 'Account number level {level}',
  'pages.account-number.form.add-account': 'Add account level {level}',
  'pages.account-number.success': '{title} account number successful',
};
