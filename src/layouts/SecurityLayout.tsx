import React from 'react';
import { PageLoading } from '@ant-design/pro-layout';
import type { ConnectProps } from 'umi';
import { Redirect, connect } from 'umi';
import { stringify } from 'querystring';
import type { ConnectState } from '@/models/connect';
import type { CurrentUser } from '@/models/user';

type SecurityLayoutProps = {
  children: React.ReactElement;
  loading?: boolean;
  currentUser?: CurrentUser;
  accessToken?: string;
} & ConnectProps;

type SecurityLayoutState = {
  isReady: boolean;
};

const SecurityLayout: React.FC<SecurityLayoutProps> = (props) => {
  const [state, setState] = React.useState<SecurityLayoutState>({ isReady: false });
  const { children, loading, currentUser, accessToken, dispatch } = props;

  const { isReady } = state;
  const queryString = stringify({
    redirect: window.location.href,
  });

  if (accessToken) {
    return children;
  }

  if ((!accessToken && loading) || !isReady) {
    return <PageLoading />;
  }
  if (!accessToken && window.location.pathname !== '/user/login') {
    return <Redirect to={`/user/login?${queryString}`} />;
  }
};

export default connect(({ user, auth, loading }: ConnectState) => ({
  currentUser: user.currentUser,
  accessToken: auth.accessToken,
  loading: loading.models.user,
}))(SecurityLayout);
