import React from 'react';
import { ConfigProvider } from 'antd';
import { Inspector } from 'react-dev-inspector';
import { setLocale } from 'umi';
import viVN from 'antd/lib/locale/vi_VN';

const InspectorWrapper = process.env.NODE_ENV === 'development' ? Inspector : React.Fragment;
setLocale('vi-VN', true);
const Layout: React.FC = ({ children }) => {
  return (
    <InspectorWrapper>
      <ConfigProvider locale={viVN}>{children}</ConfigProvider>
    </InspectorWrapper>
  );
};

export default Layout;
