import request from '@/utils/request';

/** Query Profile GET /api/admin */
export async function queryProfile() {
  return request<API.CurrentUser>('/api/admin', {
    method: 'GET',
  });
}