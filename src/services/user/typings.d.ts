// @ts-ignore
/* eslint-disable */

declare namespace API {
  export type Permission = {
    permissionKey: string;
    _id: string;
  };

  export type CurrentUser = {
    phone?: string;
    username?: string;
    status?: string;
    roleId?: string;
    roleInfo?: {
      status: string;
      _id: string;
      roleName: string;
      rolePermissions: Permission[];
    };
    _id?: string;
  };
}
