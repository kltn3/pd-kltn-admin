import request from '@/utils/request';

export async function queryProfile(): Promise<any> {
  return request('/api/admin');
}

export async function queryCurrent(): Promise<any> {
  return request('/api/currentUser');
}

export async function queryNotices(): Promise<any> {
  return request('/api/notices');
}
