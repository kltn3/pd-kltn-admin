import request from '@/utils/request';

/** Query Setting Banks GET /api/settingBank */
export async function queryBank(query: API.PageLimitParams) {
  return request<API.Bank[]>('/api/settingBank', {
    method: 'GET',
    data: query,
  });
}

/** Create Setting Bank POST /api/settingBank */
export async function createSettingBank(body: API.Bank) {
  return request('/api/settingBank', {
    method: 'POST',
    data: body,
  });
}

/** Update Setting Bank PUT /api/settingBank */
export async function updateSettingBank(body: API.Bank) {
  return request('/api/settingBank', {
    method: 'PUT',
    data: body,
  });
}

/** Delete Setting Bank PUT /api/settingBank */
export async function deleteSettingBank(query: { _id: string }) {
  return request(`/api/settingBank/${query._id}`, {
    method: 'DELETE',
  });
}
