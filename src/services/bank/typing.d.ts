// @ts-ignore
/* eslint-disable */

declare namespace API {
  type Bank = {
    key?: number;
    _id?: string;
    name: string;
    logo: string | API.Image;
  };
}
