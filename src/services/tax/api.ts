import request from '@/utils/request';

/** Query Taxes GET /api/settingTax */
export async function queryTaxes(query: API.PageLimitParams) {
  return request<API.Tax[]>('/api/settingTax', {
    method: 'GET',
    data: query,
  });
}

/** Create Tax POST /api/settingTax */
export async function createSettingTax(data: API.TaxRecord) {
  return request('/api/settingTax', {
    method: 'POST',
    data,
  });
}

/** Update Tax PUT /api/settingTax */
export async function updateSettingTax({ _id, taxInfo }: { _id: string; taxInfo: API.TaxRecord }) {
  return request(`/api/settingTax/${_id}`, {
    method: 'PUT',
    data: taxInfo,
  });
}

/** Delete Tax PUT /api/settingTax */
export async function deleteSettingTax({ _id }: { _id: string }) {
  return request(`/api/settingTax/${_id}`, {
    method: 'DELETE',
  });
}
