// @ts-ignore
/* eslint-disable */

declare namespace API {
  type TaxRecord = {
    key?: number;
    _id?: string;
    name?: string;
    percent?: number;
    accountNumber?: number;
  };
}
