// @ts-ignore
/* eslint-disable */

declare namespace API {
  type SystemConstants = {
    userTotalFreeCompany?: number;
    userTotalFreeBill?: number;
    userRoleFreeId?: string;
  };
}
