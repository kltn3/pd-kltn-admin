import request from '@/utils/request';

/** Query System Constants GET /api/systemConst */
export async function querySystemConstants() {
  return request<API.SystemConstants>('/api/systemConst', {
    method: 'GET',
  });
}

/** Update System Constants PUT /api/systemConst */
export async function updateSystemConstants(body: API.SystemConstants) {
  return request('/api/systemConst', {
    method: 'PUT',
    data: body,
  });
}
