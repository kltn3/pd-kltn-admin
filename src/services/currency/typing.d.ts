// @ts-ignore
/* eslint-disable */

declare namespace API {
  type Currency = {
    key?: number;
    _id?: string;
    name: string;
    defaultExchangeRate: number;
  };
}
