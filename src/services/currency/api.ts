import request from '@/utils/request';

/** Query Currency GET /api/moneyType */
export async function queryCurrency(query: API.PageLimitParams) {
  return request<API.Currency[]>('/api/moneyType', {
    method: 'GET',
    data: query,
  });
}

/** Create Currency POST /api/moneyType */
export async function createCurrency(body: API.Currency) {
  return request('/api/moneyType', {
    method: 'POST',
    data: body,
  });
}

/** Update Currency PUT /api/moneyType */
export async function updateCurrency(data: { _id: string; data: API.Currency }) {
  return request(`/api/moneyType/${data._id}`, {
    method: 'PUT',
    data: data.data,
  });
}

/** Delete Currency PUT /api/moneyType */
export async function deleteCurrency(query: { _id: string }) {
  return request(`/api/moneyType/${query._id}`, {
    method: 'DELETE',
  });
}
