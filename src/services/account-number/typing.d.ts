// @ts-ignore
/* eslint-disable */

declare namespace API {
  type AccountNumber = {
    key: string | React.ReactText;
    accountName?: string;
    accountNumber?: number;
    parentNumber?: number;
    _id?: string;
    level?: number;
    children?: AccountNumber[];
  };

  type AccountNumberRecord = {
    key: number;
    accountNumbers: AccountNumber[];
    circulars: string;
    _id?: string;
  };
}
