import request from '@/utils/request';

/** Query Account Number GET /api/accountNumber */
export async function queryAccountNumber() {
  return request<API.AccountNumberRecord[]>('/api/accountNumber', {
    method: 'GET',
  });
}

/** Create Account Number POST /api/accountNumber */
export async function createAccountNumber(body: API.AccountNumberRecord) {
  return request('/api/accountNumber', {
    method: 'POST',
    data: body,
  });
}

/** Update Account Number PUT /api/accountNumber */
export async function updateAccountNumber(data: { _id: string; data: API.AccountNumberRecord }) {
  return request(`/api/accountNumber/${data._id}`, {
    method: 'PUT',
    data: data.data,
  });
}

/** Delete Account Number PUT /api/accountNumber/:id */
export async function deleteAccountNumber(query: { _id: string }) {
  return request(`/api/accountNumber/${query._id}`, {
    method: 'DELETE',
  });
}
