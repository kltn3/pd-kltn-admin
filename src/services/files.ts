import request from '@/utils/request';

/** Upload Image POST /api/files/images */
export async function uploadImage(file: File | Blob) {
  const formData = new FormData();
  formData.append('file', file);
  return request<File | Blob>('/api/files/images', {
    method: 'POST',
    data: formData,
  });
}
