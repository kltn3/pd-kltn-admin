import request from '@/utils/request';

/** LogIn POST /api/admin/login */
export async function login(body: API.LoginParams) {
  return request('/api/admin/login', {
    method: 'POST',
    data: body,
  });
}