import request from '@/utils/request';

/** Query CompanyType GET /api/companyType */
export async function queryCompanyType({ limit = 10, page = 0 }: API.PageLimitParams) {
  return request<API.CompanyType[]>(`/api/companyType?limit=${limit}&page=${page}`, {
    method: 'GET',
  });
}

/** Create CompanyType POST /api/companyType */
export async function createCompanyType(body: API.CompanyType) {
  return request('/api/companyType', {
    method: 'POST',
    data: body,
  });
}

/** Update CompanyType PUT /api/companyType */
export async function updateCompanyType(data: { _id: string; data: API.CompanyType }) {
  return request(`/api/companyType/${data._id}`, {
    method: 'PUT',
    data: data.data,
  });
}

/** Delete CompanyType PUT /api/companyType */
export async function deleteCompanyType(query: { _id: string }) {
  return request(`/api/companyType/${query._id}`, {
    method: 'DELETE',
  });
}
