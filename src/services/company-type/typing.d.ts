// @ts-ignore
/* eslint-disable */

declare namespace API {
  type CompanyType = {
    key?: number;
    _id?: string;
    name: string;
  };
}
