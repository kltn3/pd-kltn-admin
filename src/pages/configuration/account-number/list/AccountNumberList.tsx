import * as React from 'react';
import { Modal, Card } from 'antd';
import { ExclamationCircleOutlined } from '@ant-design/icons';
import Hotkeys from 'react-hot-keys';
import { ConnectState } from '@/models/connect';
import { Dispatch, connect, FormattedMessage, IRouteComponentProps } from 'umi';
import { AccountNumberTable } from '../table';
import { PageContainer } from '@ant-design/pro-layout';

interface AccountNumberListProps extends IRouteComponentProps {
  dispatch: Dispatch;
  loading: boolean;
  accountNumberRecords: API.AccountNumberRecord[];
}

const AccountNumberList: React.FC<AccountNumberListProps> = (props) => {
  const { accountNumberRecords, loading, dispatch } = props;

  const [selectedRowKeys, setSelectedRowKeys] = React.useState<number[]>([]);
  const hasSelected = selectedRowKeys.length > 0;
  const onSelectRowChange = (selectedRowKeys: any, selectedRows: any) => {
    setSelectedRowKeys(selectedRowKeys);
  };

  React.useEffect(() => {
    dispatch({
      type: 'configuration/fetchAccountNumber',
      payload: {
        page: 0,
        limit: 10,
      },
    });
    // eslint-disable-next-line
  }, []);

  const onDeleteRows = () => {
    Modal.confirm({
      title: 'Xác nhận',
      icon: <ExclamationCircleOutlined />,
      content: <div>Bạn có chắc chắn xóa {selectedRowKeys.length} dòng dữ liệu này</div>,
      okText: 'Xóa',
      cancelText: 'Hủy',
      onOk() {
        Modal.destroyAll();
      },
      onCancel() {
        console.log('Cancel');
      },
    });
  };

  const onKeyDown = (keyName: any, e: any, handle: any) => {
    switch (keyName) {
      case 'shift+n':
        props.history.push('/config/account-number/create');
        break;
      case 'shift+a':
        if (hasSelected) {
          setSelectedRowKeys([]);
        } else {
          setSelectedRowKeys(accountNumberRecords.map((item) => item.key));
        }
        break;
      case 'delete':
        onDeleteRows();
        break;

      default:
        break;
    }
  };

  return (
    <React.Fragment>
      <PageContainer
        title={
          <FormattedMessage id="menu.accounting-number" defaultMessage="Accounting Number" />
        }
      >
        <Card
          title={
            <FormattedMessage id="pages.account-number.list" defaultMessage="Account Number List" />
          }
          className="card"
          bordered={false}
        >
          <Hotkeys keyName="shift+a,shift+n,delete" onKeyDown={onKeyDown}>
            <AccountNumberTable
              accountNumberRecords={accountNumberRecords}
              loading={loading}
              dispatch={dispatch}
              selectedRowKeys={selectedRowKeys}
              onSelectRowChange={onSelectRowChange}
            />
          </Hotkeys>
        </Card>
      </PageContainer>
    </React.Fragment>
  );
};

export default connect(({ configuration, loading }: ConnectState) => ({
  accountNumberRecords: configuration.accountNumberRecords,
  loading:
    loading.effects['configuration/fetchAccountNumber'] ||
    loading.effects['configuration/deleteAccountNumber'],
}))(AccountNumberList);
