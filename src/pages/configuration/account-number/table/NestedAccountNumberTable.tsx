import React from 'react';
import {
  Button,
  Form,
  Input,
  InputNumber,
  Popconfirm,
  Space,
  Table,
  TableColumnType,
  Tooltip,
} from 'antd';
import { EditOutlined, DeleteOutlined } from '@ant-design/icons';
import { Dispatch, FormattedMessage } from 'umi';
import { blue, red } from '@ant-design/colors';

interface NestedAccountNumberTableProps {
  record: API.AccountNumberRecord;
  dispatch: Dispatch;
}

interface EditableCellProps extends React.HTMLAttributes<HTMLElement> {
  editing: boolean;
  dataIndex: string;
  title: any;
  inputType: 'number' | 'text';
  record: API.AccountNumber;
  index: number;
  children: React.ReactNode;
}

const EditableCell: React.FC<EditableCellProps> = ({
  editing,
  dataIndex,
  title,
  inputType,
  record,
  index,
  children,
  ...restProps
}) => {
  const inputNode = inputType === 'number' ? <InputNumber /> : <Input />;

  return (
    <td {...restProps}>
      {editing ? (
        <Form.Item
          name={dataIndex}
          style={{ margin: 0 }}
          rules={[
            {
              required: true,
              message: `Vui lòng nhập trường dữ liệu ${title}!`,
            },
          ]}
        >
          {inputNode}
        </Form.Item>
      ) : (
        children
      )}
    </td>
  );
};

export const NestedAccountNumberEditableTable: React.FC<NestedAccountNumberTableProps> = ({
  dispatch,
  record,
}) => {
  const { accountNumbers } = record;

  const [form] = Form.useForm();
  const [data, setData] = React.useState(accountNumbers);
  const [editingKey, setEditingKey] = React.useState('');

  const isEditing = (record: API.AccountNumber) => record.key === editingKey;

  const edit = (record: Partial<API.AccountNumber> & { key: React.Key }) => {
    form.setFieldsValue({ name: '', age: '', address: '', ...record });
    setEditingKey(record.key as string);
  };

  const cancel = () => {
    setEditingKey('');
  };

  const save = async (key: React.Key) => {
    try {
      const row = (await form.validateFields()) as API.AccountNumber;

      const newData = [...data];
      const index = newData.findIndex((item) => key === item.key);
      if (index > -1) {
        const item = newData[index];
        newData.splice(index, 1, {
          ...item,
          ...row,
        });
        setData(newData);
        setEditingKey('');
      } else {
        newData.push(row);
        setData(newData);
        setEditingKey('');
      }
      const updateData: API.AccountNumber[] = [];
      newData.forEach((item) => {
        if (item.children) {
          item.children.forEach((child) => {
            updateData.push(child);
          });
        }
        updateData.push(item);
      });

      dispatch({
        type: 'configuration/updateAccountNumber',
        payload: {
          _id: record._id as string,
          data: {
            // circulars,
            accountNumbers: updateData,
          },
        },
      });
    } catch (errInfo) {
      console.log('Validate Failed:', errInfo);
    }
  };

  const remove = (record: Partial<API.AccountNumber> & { key: React.Key }) => {
    console.log('record', record);
  };

  const columns = [
    {
      title: (
        <FormattedMessage
          id="pages.account-number.column.account-number"
          defaultMessage="Account number"
        />
      ),
      editable: true,
      dataIndex: 'accountNumber',
      key: 'accountNumber',
      render: (text: string, record: API.AccountNumber) => (
        <span style={{ fontWeight: record.children ? 'bold' : 'normal' }}>{text}</span>
      ),
    },
    {
      title: (
        <FormattedMessage
          id="pages.account-number.column.account-name"
          defaultMessage="Account name"
        />
      ),
      editable: true,
      dataIndex: 'accountName',
      key: 'accountName',
      render: (text: string, record: API.AccountNumber) => (
        <span
          style={{
            color: record.children ? blue.primary : 'inherit',
            fontWeight: record.children ? 'bold' : 'normal',
          }}
        >
          {text}
        </span>
      ),
    },
    {
      title: <FormattedMessage id="pages.account-number.column.actions" defaultMessage="Actions" />,
      dataIndex: 'operation',
      render: (_: any, record: API.AccountNumber) => {
        const editable = isEditing(record);
        return editable ? (
          <Space>
            <span
              className="cursor-pointer"
              style={{ color: blue.primary }}
              onClick={() => save(record.key?.toString() as string)}
            >
              {<FormattedMessage id="component.button.save" defaultMessage="Save" />}
            </span>
            <Popconfirm
              className="text-danger"
              title={
                <FormattedMessage
                  id="component.popconfirm.cancel-operation"
                  defaultMessage="Cancel operation?"
                />
              }
              onConfirm={cancel}
            >
              <span className="cursor-pointer">
                {<FormattedMessage id="component.popconfirm.cancel" defaultMessage="Cancel" />}
              </span>
            </Popconfirm>
          </Space>
        ) : (
          <Space>
            <Tooltip
              placement="top"
              title={<FormattedMessage id="component.tooltip.edit" defaultMessage="Edit" />}
            >
              <Button
                type="text"
                disabled={editingKey !== ''}
                onClick={() => edit(record)}
                icon={
                  <EditOutlined
                    style={{
                      color: blue.primary,
                    }}
                  />
                }
              />
            </Tooltip>
            <Tooltip
              placement="top"
              title={<FormattedMessage id="component.tooltip.delete" defaultMessage="Delete" />}
            >
              <Popconfirm
                title={
                  <FormattedMessage
                    id="component.popconfirm.confirm"
                    defaultMessage="Are you sure to delete this record?"
                  />
                }
                onConfirm={() => {
                  remove(record);
                }}
                cancelText={
                  <FormattedMessage id="component.popconfirm.cancel" defaultMessage="Cancel" />
                }
                okText={<FormattedMessage id="component.tooltip.delete" defaultMessage="Delete" />}
                disabled={editingKey !== ''}
              >
                <Button
                  type="text"
                  icon={
                    <DeleteOutlined
                      style={{
                        color: red.primary,
                      }}
                    />
                  }
                />
              </Popconfirm>
            </Tooltip>
          </Space>
        );
      },
    },
  ];

  const mergedColumns = columns.map((col) => {
    if (!col.editable) {
      return col;
    }
    return {
      ...col,
      onCell: (record: API.AccountNumber) => ({
        record,
        inputType: col.dataIndex === 'accountNumber' ? 'number' : 'text',
        dataIndex: col.dataIndex,
        title: col.title,
        editing: isEditing(record),
      }),
    };
  }) as API.AccountNumber[];

  return (
    <Form form={form} component={false}>
      <Table
        components={{
          body: {
            cell: EditableCell,
          },
        }}
        bordered={true}
        dataSource={data}
        columns={mergedColumns}
        rowClassName="editable-row"
        pagination={false}
      />
    </Form>
  );
};
