import * as React from 'react';
import { Space, Table, Tooltip, Button, Popconfirm, Empty } from 'antd';
import { ColumnsType } from 'antd/lib/table';
import { EditOutlined, DeleteOutlined, PlusOutlined } from '@ant-design/icons';
import { Dispatch, FormattedMessage, history } from 'umi';
import { NestedAccountNumberEditableTable } from './NestedAccountNumberTable';
import { red, blue } from '@ant-design/colors';

const columns = ({ dispatch }: { dispatch: Dispatch }): ColumnsType<API.AccountNumberRecord> => {
  return [
    {
      title: (
        <FormattedMessage id="pages.account-number.column.circular" defaultMessage="Circular" />
      ),
      dataIndex: 'circulars',
      key: 'circulars',
    },
    {
      title: <FormattedMessage id="pages.account-number.column.actions" defaultMessage="Actions" />,
      dataIndex: 'operation',
      colSpan: 1,
      render: (text: any, record: API.AccountNumberRecord) => (
        <Space>
          <Tooltip
            placement="top"
            title={<FormattedMessage id="component.tooltip.edit" defaultMessage="Edit" />}
          >
            <Button
              type="text"
              onClick={() => {
                dispatch({
                  type: 'configuration/handleCurrentAccountNumber',
                  payload: {
                    type: 'update',
                    accountNumberRecord: record,
                  },
                });
                history.push(`/config/account-number/edit/${record._id}`);
              }}
              icon={<EditOutlined style={{ color: blue.primary }} />}
            />
          </Tooltip>
          <Tooltip
            placement="top"
            title={<FormattedMessage id="component.tooltip.delete" defaultMessage="Delete" />}
          >
            <Popconfirm
              title={
                <FormattedMessage
                  id="component.popconfirm.confirm"
                  defaultMessage="Are you sure to delete this record?"
                />
              }
              onConfirm={() => {
                dispatch({
                  type: 'configuration/deleteAccountNumber',
                  payload: {
                    _id: record._id,
                  },
                });
                dispatch({
                  type: 'configuration/handleCurrentAccountNumber',
                  payload: {
                    type: 'delete',
                    accountNumberRecord: record,
                  },
                });
              }}
              cancelText={
                <FormattedMessage id="component.popconfirm.cancel" defaultMessage="Cancel" />
              }
              okText={<FormattedMessage id="component.tooltip.delete" defaultMessage="Delete" />}
            >
              <Button
                type="text"
                icon={
                  <DeleteOutlined
                    style={{
                      color: red.primary,
                    }}
                  />
                }
              />
            </Popconfirm>
          </Tooltip>
        </Space>
      ),
    },
  ];
};

interface AccountNumberTableProps {
  dispatch: Dispatch;
  loading: boolean;
  accountNumberRecords: API.AccountNumberRecord[];
  selectedRowKeys: number[];
  onSelectRowChange: (selectedRowKeys: any, selectedRows: any) => void;
}

export const AccountNumberTable: React.FC<AccountNumberTableProps> = ({
  dispatch,
  loading,
  accountNumberRecords,
  selectedRowKeys,
  onSelectRowChange,
}) => {
  return (
    <>
      <Table
        locale={{
          emptyText: <Empty description="Không có dữ liệu" />,
        }}
        bordered={true}
        columns={columns({ dispatch })}
        // rowSelection={{ selectedRowKeys, onChange: onSelectRowChange }}
        loading={loading}
        expandable={{
          expandedRowRender: (record) => (
            <NestedAccountNumberEditableTable dispatch={dispatch} record={record} />
          ),
        }}
        pagination={false}
        dataSource={accountNumberRecords}
      />
      <Button
        style={{ width: '100%', marginTop: 16, marginBottom: 8 }}
        type="dashed"
        onClick={() => history.push('/config/account-number/create')}
      >
        <PlusOutlined />
        &nbsp;
        <FormattedMessage id="pages.account-number.create" defaultMessage="Create Account Number" />
        &nbsp;(shift+n)
      </Button>
    </>
  );
};
