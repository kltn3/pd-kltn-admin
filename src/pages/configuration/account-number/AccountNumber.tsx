import React from 'react';
import type { FC } from 'react';

import { PageContainer } from '@ant-design/pro-layout';

interface AccountNumberProps {}

const AccountNumber: FC<AccountNumberProps> = (props) => {
  const { children } = props;
  return <PageContainer>{children}</PageContainer>;
};

export default AccountNumber;
