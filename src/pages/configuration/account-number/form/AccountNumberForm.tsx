import React from 'react';
import { Row, Col, Input, Button, Form, Space, InputNumber, Divider, Card } from 'antd';
import { Dispatch, FormattedMessage, useIntl, connect, useParams, history } from 'umi';
import { MinusCircleOutlined, PlusOutlined } from '@ant-design/icons';
import { PageContainer } from '@ant-design/pro-layout';
import { ConnectState } from '@/models/connect';
import { AccountNumberChildForm } from './components';

interface CreateOrEditAccountNumber {
  dispatch: Dispatch;
  submitting: boolean;
  currentAccountNumber?: API.AccountNumberRecord;
  accountNumberRecords?: API.AccountNumberRecord[];
  isUpdatingAccountNumber?: boolean;
}

export const CreateOrEditAccountNumber: React.FC<CreateOrEditAccountNumber> = (props) => {
  const [formInstance] = Form.useForm();
  const param: { circularsId: string } = useParams();
  const { submitting, dispatch, currentAccountNumber, isUpdatingAccountNumber } = props;
  const intl = useIntl();

  React.useEffect(() => {
    if (param.circularsId && currentAccountNumber) {
      formInstance.setFieldsValue({
        circulars: currentAccountNumber.circulars,
        accountNumbers: currentAccountNumber.accountNumbers,
      });
    }
    return () => {
      //   props.actions.handleClear(ACCOUNT_NUMBER_CLEAR.ACCOUNT_NUMBER_UPDATE);
    };
    // eslint-disable-next-line
  }, [currentAccountNumber]);

  const onFinish = (values: API.AccountNumberRecord) => {
    const { accountNumbers, circulars } = values;
    const formatAccountNumbers: {
      accountName?: string;
      accountNumber?: number;
      level: number;
      parentNumber?: number;
    }[] = [];

    accountNumbers.forEach((parent) => {
      formatAccountNumbers.push({
        accountName: parent.accountName,
        accountNumber: parent.accountNumber,
        level: 1,
      });
      parent.children?.forEach((child) => {
        formatAccountNumbers.push({
          ...child,
          level: 2,
          parentNumber: parent.accountNumber,
        });
      });
    });
    if (isUpdatingAccountNumber) {
      dispatch({
        type: 'configuration/updateAccountNumber',
        payload: {
          _id: currentAccountNumber?._id,
          data: {
            circulars,
            accountNumbers: formatAccountNumbers as API.AccountNumber[],
          },
        },
      });
    } else {
      dispatch({
        type: 'configuration/createAccountNumber',
        payload: {
          circulars,
          accountNumbers: formatAccountNumbers as API.AccountNumber[],
        },
      });
    }
  };

  return (
    <PageContainer
      title={
        <FormattedMessage id="menu.config.accounting-number" defaultMessage="Accounting Number" />
      }
      content={
        <FormattedMessage
          id="menu.config.accounting-number.create"
          defaultMessage="Create new account number"
        />
      }
    >
      <Card
        title={
          <FormattedMessage
            id="menu.config.accounting-number.create"
            defaultMessage="Create new account number"
          />
        }
        className="card"
        bordered={false}
      >
        {/* <Title level={4}>
        {param.circularsId && currentAccountNumber
          ? `Chỉnh sửa tài khoản kế toán - ${currentAccountNumber.circulars}`
          : 'Tạo mới tài khoản kế toán'}
      </Title> */}
        <Form form={formInstance} layout="vertical" onFinish={onFinish} scrollToFirstError={true}>
          <Form.Item
            label={
              <FormattedMessage
                id="pages.account-number.column.circular"
                defaultMessage="Circulars"
              />
            }
            name="circulars"
            rules={[
              {
                required: true,
                message: 'Vui lòng tên thông tư!',
              },
              {
                max: 256,
              },
            ]}
          >
            <Input allowClear={true} />
          </Form.Item>
          <Form.List
            name="accountNumbers"
            rules={[
              {
                validator: async (_, names) => {
                  if (!names || names.length < 1) {
                    return Promise.reject(new Error('Có ít nhất 1 tài khoản'));
                  }
                },
              },
            ]}
          >
            {(fields, { add, remove }, { errors }) => {
              return (
                <>
                  {fields.length > 0 && <Divider />}
                  {fields.map((field) => (
                    <Row key={field.key} gutter={16}>
                      <Col lg={10}>
                        <Form.Item
                          {...field}
                          label={intl.formatMessage(
                            {
                              id: 'pages.account-number.form.account-name',
                              defaultMessage: 'Account name level 1',
                            },
                            { level: '1' },
                          )}
                          name={[field.name, 'accountName']}
                          fieldKey={[field.fieldKey, 'accountName']}
                          rules={[
                            {
                              required: true,
                              message: 'Vui lòng tên tài khoản!',
                            },
                          ]}
                        >
                          <Input allowClear={true} />
                        </Form.Item>
                      </Col>

                      <Col lg={10}>
                        <Form.Item
                          {...field}
                          label={intl.formatMessage(
                            {
                              id: 'pages.account-number.form.account-number',
                              defaultMessage: 'Account number level 1',
                            },
                            { level: '1' },
                          )}
                          name={[field.name, 'accountNumber']}
                          fieldKey={[field.fieldKey, 'accountNumber']}
                          rules={[
                            {
                              required: true,
                              message: 'Vui lòng số tài khoản!',
                            },
                          ]}
                        >
                          <InputNumber className="w-100" min={0} step={1} />
                        </Form.Item>
                      </Col>

                      <Col lg={4} className="d-flex align-items-center">
                        <MinusCircleOutlined
                          onClick={() => {
                            remove(field.name);
                          }}
                        />
                      </Col>

                      <Col lg={24}>
                        <AccountNumberChildForm field={field} />
                      </Col>
                      <Divider />
                    </Row>
                  ))}
                  <Form.Item>
                    <Button
                      type="dashed"
                      onClick={() => {
                        add();
                      }}
                      block={true}
                    >
                      <PlusOutlined />
                      &nbsp;
                      {intl.formatMessage(
                        {
                          id: 'pages.account-number.form.add-account',
                          defaultMessage: 'Add account number',
                        },
                        { level: '1' },
                      )}
                    </Button>
                    <Form.ErrorList errors={errors} />
                  </Form.Item>
                </>
              );
            }}
          </Form.List>

          <div className="d-flex w-100 justify-content-end">
            <Space>
              <Button
                className="text-capitalize"
                onClick={() => history.push('/config/account-number/list')}
              >
                <FormattedMessage id="component.button.back" defaultMessage="Back" />
              </Button>
              <Button
                className="text-capitalize"
                type="primary"
                htmlType="submit"
                loading={submitting}
              >
                <FormattedMessage
                  id={
                    isUpdatingAccountNumber ? 'component.button.update' : 'component.button.create'
                  }
                  defaultMessage={isUpdatingAccountNumber ? 'Update' : 'Create'}
                />
              </Button>
            </Space>
          </div>
        </Form>
      </Card>
    </PageContainer>
  );
};

export default connect(({ configuration, loading }: ConnectState) => ({
  currentAccountNumber: configuration.currentAccountNumber,
  isUpdatingAccountNumber: configuration.isUpdatingAccountNumber,
  submitting:
    loading.effects['configuration/createAccountNumber'] ||
    loading.effects['configuration/updateAccountNumber'],
}))(CreateOrEditAccountNumber);
