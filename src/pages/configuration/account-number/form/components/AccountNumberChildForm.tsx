import React from 'react';
import { Form, Row, Col, Divider, Button, Input, InputNumber } from 'antd';
import { MinusCircleOutlined, PlusOutlined } from '@ant-design/icons';
import { FormListFieldData } from 'antd/lib/form/FormList';
import { useIntl } from 'react-intl';

export const AccountNumberChildForm = ({ field }: { field: FormListFieldData }) => {
  const intl = useIntl();
  return (
    <Form.List name={[field.name, 'children']}>
      {(fields, { add, remove }) => {
        return (
          <>
            <Divider orientation="left">
              {intl.formatMessage(
                {
                  id: 'pages.account-number.form.account-list-level',
                  defaultMessage: 'Account list',
                },
                { level: '2' },
              )}
            </Divider>
            {fields.map((field) => (
              <Row key={field.key} gutter={16}>
                <Col lg={10}>
                  <Form.Item
                    {...field}
                    label={intl.formatMessage(
                      {
                        id: 'pages.account-number.form.account-name',
                        defaultMessage: 'Account name level 2',
                      },
                      { level: '2' },
                    )}
                    name={[field.name, 'accountName']}
                    fieldKey={[field.fieldKey, 'accountName']}
                    rules={[
                      {
                        required: true,
                        message: 'Vui lòng tên tài khoản!',
                      },
                    ]}
                  >
                    <Input allowClear={true} />
                  </Form.Item>
                </Col>

                <Col lg={10}>
                  <Form.Item
                    {...field}
                    label={intl.formatMessage(
                      {
                        id: 'pages.account-number.form.account-number',
                        defaultMessage: 'Account number level 2',
                      },
                      { level: '2' },
                    )}
                    name={[field.name, 'accountNumber']}
                    fieldKey={[field.fieldKey, 'accountNumber']}
                    rules={[
                      {
                        required: true,
                        message: 'Vui lòng số tài khoản!',
                      },
                    ]}
                  >
                    <InputNumber className="w-100" min={0} step={1} />
                  </Form.Item>
                </Col>

                <Col lg={2} className="d-flex align-items-center">
                  <MinusCircleOutlined
                    onClick={() => {
                      remove(field.name);
                    }}
                  />
                </Col>
              </Row>
            ))}
            <Form.Item>
              <Button
                type="dashed"
                onClick={() => {
                  add();
                }}
                block={true}
              >
                <PlusOutlined />
                &nbsp;
                {intl.formatMessage(
                  {
                    id: 'pages.account-number.form.add-account',
                    defaultMessage: 'Add account number',
                  },
                  { level: '2' },
                )}
              </Button>
            </Form.Item>
          </>
        );
      }}
    </Form.List>
  );
};
