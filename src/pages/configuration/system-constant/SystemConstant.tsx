import React, { useEffect, useState } from 'react';
import type { FC } from 'react';
import { CloseCircleOutlined } from '@ant-design/icons';
import { Button, Card, Col, Form, InputNumber, Popover, Row } from 'antd';
import { PageContainer, FooterToolbar } from '@ant-design/pro-layout';
import { Dispatch, FormattedMessage } from 'umi';
import { connect } from 'umi';
// import TableForm from './components/TableForm';
import styles from './style.less';
import { ConnectState } from '@/models/connect';

type InternalNamePath = (string | number)[];

const fieldLabels = {
  userTotalFreeCompany: 'Total Free Company',
  userTotalFreeBill: 'Total Free Bill',
};

interface SystemConstantProps {
  dispatch: Dispatch;
  submitting: boolean;
  systemConstants: API.SystemConstants;
}

interface ErrorField {
  name: InternalNamePath;
  errors: string[];
}

const SystemConstant: FC<SystemConstantProps> = (props) => {
  const [form] = Form.useForm();
  const { submitting, dispatch, systemConstants } = props;
  const [error, setError] = useState<ErrorField[]>([]);
  const initialValues = {
    userTotalFreeCompany: 1,
    userTotalFreeBill: 1,
  };

  useEffect(() => {
    if (!systemConstants) {
      dispatch({
        type: 'configuration/fetchSystemConstants',
      });
    }
  }, [systemConstants]);

  const getErrorInfo = (errors: ErrorField[]) => {
    const errorCount = errors.filter((item) => item.errors.length > 0).length;
    if (!errors || errorCount === 0) {
      return null;
    }

    const scrollToField = (fieldKey: string) => {
      const labelNode = document.querySelector(`label[for="${fieldKey}"]`);
      if (labelNode) {
        labelNode.scrollIntoView(true);
      }
    };

    const errorList = errors.map((err) => {
      if (!err || err.errors.length === 0) {
        return null;
      }
      const key = err.name[0] as string;
      return (
        <li key={key} className={styles.errorListItem} onClick={() => scrollToField(key)}>
          <CloseCircleOutlined className={styles.errorIcon} />
          <div className={styles.errorMessage}>{err.errors[0]}</div>
          <div className={styles.errorField}>{fieldLabels[key]}</div>
        </li>
      );
    });
    return (
      <span className={styles.errorIcon}>
        <Popover
          title="Error Notification"
          content={errorList}
          overlayClassName={styles.errorPopover}
          trigger="click"
          getPopupContainer={(trigger: HTMLElement) => {
            if (trigger && trigger.parentNode) {
              return trigger.parentNode as HTMLElement;
            }
            return trigger;
          }}
        >
          <CloseCircleOutlined />
        </Popover>
        {errorCount}
      </span>
    );
  };

  const onFinish = (values: Record<string, any>) => {
    setError([]);
    dispatch<API.SystemConstants>({
      type: 'configuration/updateSystemConstants',
      payload: values,
    });
  };

  const onFinishFailed = (errorInfo: any) => {
    setError(errorInfo.errorFields);
  };

  return (
    <Form
      form={form}
      layout="vertical"
      hideRequiredMark
      initialValues={systemConstants ? systemConstants : initialValues}
      onFinish={onFinish}
      onFinishFailed={onFinishFailed}
    >
      <PageContainer>
        <Card
          title={
            <FormattedMessage
              id="pages.systemConstants.freePackage"
              defaultMessage="Free Package Management"
            />
          }
          className={styles.card}
          bordered={false}
        >
          <Row gutter={16}>
            <Col lg={6} md={12} sm={24}>
              <Form.Item
                label={fieldLabels.userTotalFreeCompany}
                name="userTotalFreeCompany"
                rules={[
                  {
                    type: 'integer',
                    message: 'Vui nhập số',
                  },
                  {
                    required: true,
                  },
                ]}
              >
                <InputNumber
                  style={{ width: '50%' }}
                  placeholder="Total Free Company"
                  min={1}
                  max={999999}
                />
              </Form.Item>
            </Col>
            <Col lg={6} md={12} sm={24}>
              <Form.Item
                label={fieldLabels.userTotalFreeBill}
                name="userTotalFreeBill"
                rules={[
                  {
                    type: 'integer',
                    message: 'Vui nhập số',
                  },
                  {
                    required: true,
                  },
                ]}
              >
                <InputNumber
                  style={{ width: '50%' }}
                  placeholder="Total Free Bill"
                  min={1}
                  max={999999}
                />
              </Form.Item>
            </Col>
          </Row>
        </Card>
      </PageContainer>
      <FooterToolbar>
        {getErrorInfo(error)}
        <Button type="primary" onClick={() => form?.submit()} loading={submitting}>
          <FormattedMessage id="component.button.update" defaultMessage="Update" />
        </Button>
      </FooterToolbar>
    </Form>
  );
};

export default connect(({ configuration, loading }: ConnectState) => ({
  systemConstants: configuration.systemConstants,
  submitting: loading.effects['configuration/updateSystemConstants'],
}))(SystemConstant);
