import React, { useEffect, useState } from 'react';
import type { FC } from 'react';
import { PlusOutlined } from '@ant-design/icons';
import { Button, Divider, Input, Popconfirm, Table, message, Card, Space } from 'antd';
import { PageContainer } from '@ant-design/pro-layout';
import styles from './style.less';
import { ColumnsType } from 'antd/lib/table';
import { blue, red } from '@ant-design/colors';
import { ConnectState } from '@/models/connect';
import { FormattedMessage, useIntl, connect, Dispatch } from 'umi';
interface CompanyTypeFormType {
  _id?: string;
  key: string;
  name?: string;
  isNew?: boolean;
  editable?: boolean;
  align?: 'left' | 'center' | 'right';
}
interface CompanyTypeProps {
  companyTypeRecords?: CompanyTypeFormType[];
  dispatch: Dispatch;
  processing: boolean;
}

const CompanyType: FC<CompanyTypeProps> = ({ processing, companyTypeRecords, dispatch }) => {
  const [clickedCancel, setClickedCancel] = useState(false);
  const [index, setIndex] = useState(0);
  const [cacheOriginData, setCacheOriginData] = useState({});
  const [data, setData] = useState(companyTypeRecords);
  const intl = useIntl();

  useEffect(() => {
    dispatch({
      type: 'configuration/fetchCompanyType',
      payload: {
        limit: 10,
        page: 0,
      },
    });
  }, []);

  useEffect(() => {
    if (companyTypeRecords) {
      setData(companyTypeRecords);
    }
  }, [companyTypeRecords]);

  const getRowByKey = (key: string, newData?: CompanyTypeFormType[]) =>
    (newData || data)?.filter((item) => item.key === key)[0];

  const toggleEditable = (e: React.MouseEvent | React.KeyboardEvent, key: string) => {
    e.preventDefault();
    const newData = data?.map((item) => ({ ...item }));
    const target = getRowByKey(key, newData);
    if (target) {
      // Lưu dữ liệu gốc khi vào trạng thái chỉnh sửa
      if (!target.editable) {
        cacheOriginData[key] = { ...target };
        setCacheOriginData(cacheOriginData);
      }
      target.editable = !target.editable;
      setData(newData);
    }
  };
  const newMember = () => {
    const newData = data?.map((item) => ({ ...item })) || [];

    newData.push({
      key: `tax_record_${index}`,
      name: '',
      editable: true,
      isNew: true,
    });

    setIndex(index + 1);
    setData(newData);
  };

  const remove = (key: string) => {
    const newData = data?.filter((item) => item.key !== key) as CompanyTypeFormType[];
    setData(newData);
  };

  const handleFieldChange = (
    e: React.ChangeEvent<HTMLInputElement>,
    fieldName: string,
    key: string,
  ) => {
    const newData = [...(data as CompanyTypeFormType[])];
    const target = getRowByKey(key, newData);
    if (target) {
      target[fieldName] = e.target.value;
      setData(newData);
    }
  };

  const saveRow = (e: React.MouseEvent | React.KeyboardEvent, record: CompanyTypeFormType) => {
    e.persist();
    if (clickedCancel) {
      setClickedCancel(false);
      return;
    }
    const target = getRowByKey(record.key) || ({} as any);
    if (!target.name) {
      message.error(
        intl.formatMessage({
          id: 'component.input.validation.empty',
          defaultMessage: 'Please enter input value',
        }),
      );
      (e.target as HTMLInputElement).focus();
      return;
    }
    delete target.isNew;
    toggleEditable(e, record.key);
    if (record._id) {
      dispatch({
        type: 'configuration/updateCompanyType',
        payload: {
          _id: record._id,
          data: {
            name: target.name,
          },
        },
      });
      return;
    }
    dispatch({
      type: 'configuration/createCompanyType',
      payload: {
        name: target.name,
      },
    });
  };

  const handleKeyPress = (e: React.KeyboardEvent, record: CompanyTypeFormType) => {
    if (e.key === 'Enter') {
      saveRow(e, record);
    }
  };

  const cancel = (e: React.MouseEvent, key: string) => {
    setClickedCancel(true);
    e.preventDefault();
    const newData = [...(data as CompanyTypeFormType[])];
    // Caching data
    let cacheData = [];
    cacheData = newData.map((item) => {
      if (item.key === key) {
        if (cacheOriginData[key]) {
          const originItem = {
            ...item,
            ...cacheOriginData[key],
            editable: false,
          };
          delete cacheOriginData[key];
          setCacheOriginData(cacheOriginData);
          return originItem;
        }
      }
      return item;
    });
    setData(cacheData);
    setClickedCancel(false);
  };

  const columns: ColumnsType<CompanyTypeFormType> = [
    {
      title: <FormattedMessage id="pages.company-type.column.name" defaultMessage="Company Type" />,
      dataIndex: 'name',
      key: 'name',
      width: '30%',
      render: (text: string, record: CompanyTypeFormType) => {
        if (record.editable) {
          return (
            <Input
              value={text}
              autoFocus
              onChange={(e) => handleFieldChange(e, 'name', record.key)}
              onKeyPress={(e) => handleKeyPress(e, record)}
              placeholder={useIntl().formatMessage({
                id: 'pages.company-type.column.name',
                defaultMessage: 'Company Type',
              })}
            />
          );
        }
        return text;
      },
    },
    {
      title: <FormattedMessage id="component.table.column.actions" defaultMessage="Actions" />,
      key: 'action',
      align: 'right',
      render: (text: string, record: CompanyTypeFormType) => {
        if (!!record.editable && processing) {
          return null;
        }
        if (record.editable) {
          if (record.isNew) {
            return (
              <Space>
                <span
                  className="cursor-pointer"
                  style={{ color: blue.primary }}
                  onClick={(e) => saveRow(e, record)}
                >
                  <FormattedMessage id="component.button.save" defaultMessage="Save" />
                </span>
                <Divider type="vertical" />
                <Popconfirm
                  title={
                    <FormattedMessage
                      id="component.popconfirm.confirm"
                      defaultMessage="Are you sure to delete this record?"
                    />
                  }
                  onConfirm={() => remove(record.key)}
                >
                  <span className="cursor-pointer" style={{ color: red.primary }}>
                    <FormattedMessage id="component.button.remove" defaultMessage="Remove" />
                  </span>
                </Popconfirm>
              </Space>
            );
          }
          return (
            <Space>
              <span
                className="cursor-pointer"
                style={{ color: blue.primary }}
                onClick={(e) => saveRow(e, record)}
              >
                <FormattedMessage id="component.button.save" defaultMessage="Save" />
              </span>
              <Divider type="vertical" />
              <span className="cursor-pointer" onClick={(e) => cancel(e, record.key)}>
                <FormattedMessage id="component.popconfirm.cancel" defaultMessage="Cancel" />
              </span>
            </Space>
          );
        }
        return (
          <Space>
            <span
              className="cursor-pointer"
              style={{ color: blue.primary }}
              onClick={(e) => toggleEditable(e, record.key)}
            >
              <FormattedMessage id="component.tooltip.edit" defaultMessage="Edit" />
            </span>
            <Divider type="vertical" />
            <Popconfirm
              title={
                <FormattedMessage
                  id="component.popconfirm.confirm"
                  defaultMessage="Are you sure to delete this record?"
                />
              }
              onConfirm={() => {
                dispatch({
                  type: 'configuration/deleteCompanyType',
                  payload: {
                    _id: record._id,
                  },
                });
              }}
            >
              <span className="cursor-pointer" style={{ color: red.primary }}>
                <FormattedMessage id="component.tooltip.delete" defaultMessage="Delete" />
              </span>
            </Popconfirm>
          </Space>
        );
      },
    },
  ];

  return (
    <PageContainer>
      <Card
        title={<FormattedMessage id="pages.company-type.list" defaultMessage="Currency List" />}
        className="card"
        bordered={false}
      >
        <Table
          loading={processing}
          columns={columns}
          dataSource={data}
          pagination={false}
          rowClassName={(record) => (record.editable ? styles.editable : '')}
        />
        <Button
          style={{ width: '100%', marginTop: 16, marginBottom: 8 }}
          type="dashed"
          onClick={newMember}
        >
          <PlusOutlined />
          &nbsp;
          <FormattedMessage id="pages.company-type.create" defaultMessage="Create Currency" />
        </Button>
      </Card>
    </PageContainer>
  );
};

export default connect(({ configuration, loading }: ConnectState) => ({
  companyTypeRecords: configuration.companyTypeRecords,
  processing:
    loading.effects['configuration/fetchCompanyType'] ||
    loading.effects['configuration/createCompanyType'] ||
    loading.effects['configuration/updateCompanyType'] ||
    loading.effects['configuration/deleteCompanyType'],
}))(CompanyType);
