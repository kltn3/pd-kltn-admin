/* eslint-disable radix */
import React, { useState, useRef } from 'react';
import type { ActionType, ProColumns } from '@ant-design/pro-table';
import { EditableProTable } from '@ant-design/pro-table';
import { Button, Popconfirm, Space, Tooltip } from 'antd';
import type { Dispatch } from 'umi';
import { connect, FormattedMessage } from 'umi';
import { PageContainer } from '@ant-design/pro-layout';
import type { ConnectState } from '@/models/connect';
import { v4 as uuidv4 } from 'uuid';
import {
  CheckOutlined,
  CloseOutlined,
  DeleteOutlined,
  EditOutlined,
  SnippetsOutlined,
} from '@ant-design/icons';
import { blue, green, red } from '@ant-design/colors';

import styles from './style.less';

type DataSourceType = {
  isNew?: boolean;
} & API.CompanyType;

interface CompanyTypeManagementProps {
  dispatch: Dispatch;
  companyTypeRecords: DataSourceType[];
  loading: boolean;
  submitting: boolean;
}

const CompanyTypeManagement: React.FC<CompanyTypeManagementProps> = (props) => {
  const { dispatch, companyTypeRecords, loading, submitting } = props;
  const [editableKeys, setEditableRowKeys] = useState<React.Key[]>([]);
  const [dataSource, setDataSource] = useState<DataSourceType[]>([]);
  const actionRef = useRef<ActionType>();

  React.useEffect(() => {
    if (companyTypeRecords) {
      setDataSource(companyTypeRecords);
    }
  }, [companyTypeRecords]);

  React.useEffect(() => {
    dispatch({
      type: 'configuration/fetchCompanyType',
      payload: {
        limit: 10,
        page: 0,
      },
    });
  }, []);

  const columns: ProColumns<DataSourceType>[] = [
    {
      title: 'Tên loại hình công ty',
      dataIndex: 'name',
      fieldProps: {
        rows: 1,
      },
    },
    {
      title: <FormattedMessage id="component.table.column.actions" defaultMessage="Actions" />,
      dataIndex: 'operation',
      valueType: 'option',
      align: 'right',
      render: (text, record, _, action) => [
        <Space key="1">
          <Tooltip
            placement="top"
            title={<FormattedMessage id="component.tooltip.edit" defaultMessage="Edit" />}
          >
            <Button
              type="text"
              onClick={() => {
                action?.startEditable?.(record._id);
              }}
              icon={<EditOutlined style={{ color: blue.primary }} />}
            />
          </Tooltip>
          <Tooltip
            placement="top"
            title={<FormattedMessage id="component.tooltip.delete" defaultMessage="Delete" />}
          >
            <Popconfirm
              title={
                <FormattedMessage
                  id="component.popconfirm.confirm"
                  defaultMessage="Are you sure to delete this record?"
                />
              }
              onConfirm={() => {
                dispatch({
                  type: 'configuration/deleteCompanyType',
                  payload: {
                    _id: record._id,
                  },
                });
              }}
              cancelText={
                <FormattedMessage id="component.popconfirm.cancel" defaultMessage="Cancel" />
              }
              okText={<FormattedMessage id="component.tooltip.delete" defaultMessage="Delete" />}
            >
              <Button
                type="text"
                icon={
                  <DeleteOutlined
                    style={{
                      color: red.primary,
                    }}
                  />
                }
              />
            </Popconfirm>
          </Tooltip>
          <EditableProTable.RecordCreator
            key="copy"
            record={{
              ...record,
              _id: uuidv4(),
            }}
          >
            <Tooltip title="Sao chép dữ liệu xuống hàng bên dưới" placement="topRight">
              <Button type="text" icon={<SnippetsOutlined />} />
            </Tooltip>
          </EditableProTable.RecordCreator>
        </Space>,
      ],
    },
  ];

  return (
    <PageContainer>
      <EditableProTable<DataSourceType>
        rowKey="_id"
        size="small"
        loading={loading || submitting}
        actionRef={actionRef}
        className={styles.taxTable}
        headerTitle="Quản lý thông tin loại công ty"
        recordCreatorProps={{
          position: 'top',
          newRecordType: 'dataSource',
          record: () => ({ _id: uuidv4(), isNew: true }),
          creatorButtonText: 'Thêm mới dữ liệu',
        }}
        columns={columns}
        pagination={false}
        manualRequest
        value={dataSource}
        onChange={setDataSource}
        editable={{
          onlyAddOneLineAlertMessage: 'Vui lòng kết thúc thao tác để tiếp tục',
          deletePopconfirmMessage: (
            <FormattedMessage
              id="component.popconfirm.confirm"
              defaultMessage="Are you sure to delete this record?"
            />
          ),
          type: 'multiple',
          editableKeys,
          onChange: setEditableRowKeys,

          actionRender: (row, config) => [
            <Space key="1">
              <Tooltip
                placement="top"
                title={<FormattedMessage id="component.button.save" defaultMessage="Save" />}
              >
                <Button
                  type="text"
                  key="save"
                  onClick={() =>
                    config.form.validateFields().then((values) => {
                      const { name } = values[row._id] as DataSourceType;
                      const companyTypeInfo: API.CompanyType = {
                        name,
                      };
                      if (row.isNew) {
                        dispatch({
                          type: 'configuration/createCompanyType',
                          payload: companyTypeInfo,
                        });
                        return;
                      }
                      dispatch({
                        type: 'configuration/updateCompanyType',
                        payload: {
                          _id: row._id,
                          data: companyTypeInfo,
                        },
                      });
                    })
                  }
                  icon={
                    <CheckOutlined
                      style={{
                        color: green.primary,
                      }}
                    />
                  }
                />
              </Tooltip>
              <Tooltip
                placement="top"
                title={<FormattedMessage id="component.button.remove" defaultMessage="Remove" />}
              >
                <Button
                  type="text"
                  onClick={() => {
                    if (row.isNew) {
                      setDataSource(dataSource.filter((d) => d._id !== row._id));
                      setEditableRowKeys(editableKeys.filter((k) => k !== row._id));
                      return;
                    }
                    config.cancelEditable(row._id);
                  }}
                  icon={
                    <CloseOutlined
                      style={{
                        color: red.primary,
                      }}
                    />
                  }
                />
              </Tooltip>
            </Space>,
          ],
        }}
      />
    </PageContainer>
  );
};

export default connect(({ loading, configuration }: ConnectState) => ({
  companyTypeRecords: configuration.companyTypeRecords,
  loading: loading.effects['configuration/fetchCompanyType'],
  submitting:
    loading.effects['configuration/createCompanyType'] ||
    loading.effects['configuration/updateCompanyType'] ||
    loading.effects['configuration/deleteCompanyType'],
}))(CompanyTypeManagement);
