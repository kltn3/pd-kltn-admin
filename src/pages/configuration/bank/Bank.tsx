import React, { useState } from 'react';
import type { FC } from 'react';
import { UploadOutlined, PlusOutlined } from '@ant-design/icons';
import { Button, Divider, Input, Popconfirm, Table, message, Card, Space } from 'antd';
import { PageContainer } from '@ant-design/pro-layout';
import { FormattedMessage, useIntl, connect, Dispatch } from 'umi';
import { ColumnsType } from 'antd/lib/table';
import { blue, red } from '@ant-design/colors';

import styles from './style.less';
import { ConnectState } from '@/models/connect';

type BankFormType = {
  _id?: string;
  key: string;
  name?: string;
  logo?: File | Blob;
  isNew?: boolean;
  editable?: boolean;
  align?: 'left' | 'center' | 'right';
};
interface BankFormProps {
  dispatch: Dispatch;
  bankAccountRecords: BankFormType[];
  processing: boolean;
}

const BankForm: FC<BankFormProps> = ({ bankAccountRecords, dispatch, processing }) => {
  const [clickedCancel, setClickedCancel] = useState(false);
  const [index, setIndex] = useState(0);
  const [cacheOriginData, setCacheOriginData] = useState({});
  const [data, setData] = useState(bankAccountRecords);
  const [previewImages, setPreviewImages] = React.useState<
    undefined | { file: File | Blob; url: string }
  >();

  React.useEffect(() => {
    dispatch({
      type: 'configuration/fetchBank',
    });
  }, []);

  React.useEffect(() => {
    if (bankAccountRecords) {
      setData(bankAccountRecords);
    }
  }, [bankAccountRecords]);

  const getRowByKey = (key: string, newData?: BankFormType[]) =>
    (newData || data)?.filter((item) => item.key === key)[0];

  const toggleEditable = (e: React.MouseEvent | React.KeyboardEvent, key: string) => {
    e.preventDefault();
    const newData = data?.map((item) => ({ ...item }));
    const target = getRowByKey(key, newData);
    if (target) {
      // Lưu dữ liệu gốc khi vào trạng thái chỉnh sửa
      if (!target.editable) {
        cacheOriginData[key] = { ...target };
        setCacheOriginData(cacheOriginData);
      }
      target.editable = !target.editable;
      setData(newData);
    }
  };

  const newMember = () => {
    const newData = data?.map((item) => ({ ...item })) || [];

    newData.push({
      key: `tax_record_${index}`,
      name: '',
      editable: true,
      isNew: true,
    });

    setIndex(index + 1);
    setData(newData);
  };

  const remove = (record: BankFormType) => {
    const newData = data?.filter((item) => item.key !== record.key) as BankFormType[];
    setData(newData);
    URL.revokeObjectURL(previewImages?.url as string);
    setPreviewImages(undefined);
  };

  const handleFieldChange = (
    e: React.ChangeEvent<HTMLInputElement> | any,
    fieldName: string,
    key: string,
  ) => {
    const newData = [...(data as BankFormType[])];
    const target = getRowByKey(key, newData);
    if (target) {
      target[fieldName] = e.target ? e.target.value : e;
      setData(newData);
    }
  };

  const imageHandler = (record: BankFormType) => {
    const input = document.createElement('input');
    input.setAttribute('type', 'file');
    input.setAttribute('accept', 'image/*');
    input.click();
    input.onchange = () => {
      const files = input.files as FileList;
      const file = files[0];
      // file type is only image.
      if (/^image\//.test(file.type)) {
        // const url = URL.createObjectURL(file);
        // revokeObjectURL
        handleFieldChange(file, 'logo', record.key)
        // setPreviewImages({
        //   file: file,
        //   url: url,
        // });
      } else {
        message.error("Error when getting image")
      }
    };
  };

  const saveRow = (e: React.MouseEvent | React.KeyboardEvent, record: BankFormType) => {
    e.persist();

    if (clickedCancel) {
      setClickedCancel(false);
      return;
    }
    const target = getRowByKey(record.key) || ({} as any);
    if (!target.logo || !target.name) {
      message.error('Vui lòng nhập dữ liệu!');
      (e.target as HTMLInputElement).focus();
      return;
    }
    delete target.isNew;

    toggleEditable(e, record.key);
    if (record._id) {
      dispatch({
        type: 'configuration/updateBank',
        payload: {
          _id: record._id,
          data: {
            name: target.name,
            logo: target.logo,
          },
        },
      });
      return;
    }
    dispatch({
      type: 'configuration/createBank',
      payload: {
        name: target.name,
        logo: target.logo,
      },
    });
  };

  const handleKeyPress = (e: React.KeyboardEvent, record: BankFormType) => {
    if (e.key === 'Enter') {
      saveRow(e, record);
    }
  };

  const cancel = (e: React.MouseEvent, key: string) => {
    setClickedCancel(true);
    e.preventDefault();
    const newData = [...(data as BankFormType[])];
    // Data Before Edited
    let cacheData = [];
    cacheData = newData.map((item) => {
      if (item.key === key) {
        if (cacheOriginData[key]) {
          const originItem = {
            ...item,
            ...cacheOriginData[key],
            editable: false,
          };
          delete cacheOriginData[key];
          setCacheOriginData(cacheOriginData);
          return originItem;
        }
      }
      return item;
    });
    setData(cacheData);
    setClickedCancel(false);
  };

  const columns: ColumnsType<BankFormType> = [
    {
      title: <FormattedMessage id="pages.tax.column.tax" defaultMessage="Tax" />,
      dataIndex: 'name',
      key: 'name',
      width: '30%',
      render: (text: string, record: BankFormType) => {
        if (record.editable) {
          return (
            <Input
              value={text}
              autoFocus
              onChange={(e) => handleFieldChange(e, 'name', record.key)}
              onKeyPress={(e) => handleKeyPress(e, record)}
              placeholder={useIntl().formatMessage({
                id: 'pages.tax.column.tax',
                defaultMessage: 'Tax',
              })}
            />
          );
        }
        return text;
      },
    },
    {
      title: <FormattedMessage id="pages.tax.column.tax-rate" defaultMessage="Tax rate" />,
      dataIndex: 'logo',
      key: 'logo',
      width: '30%',
      render: (text: string, record: BankFormType) => {
        if (record.editable) {
          return (
            <div className="d-flex align-items-center">
              <Button onClick={() => imageHandler(record)}>
                <UploadOutlined /> Chọn hình ảnh
              </Button>
              {previewImages && (
                <div style={{ width: 120, marginLeft: 8 }}>
                  <img alt="preview" style={{ maxWidth: '100%' }} src={previewImages.url} />
                </div>
              )}
            </div>
          );
        }
        return text;
      },
    },
    {
      title: <FormattedMessage id="component.table.column.actions" defaultMessage="Actions" />,
      key: 'action',
      align: 'right',
      render: (text: string, record: BankFormType) => {
        if (!!record.editable && processing) {
          return null;
        }
        if (record.editable) {
          if (record.isNew) {
            return (
              <Space>
                <span
                  className="cursor-pointer"
                  style={{ color: blue.primary }}
                  onClick={(e) => saveRow(e, record)}
                >
                  <FormattedMessage id="component.button.save" defaultMessage="Save" />
                </span>
                <Divider type="vertical" />
                <Popconfirm
                  title={
                    <FormattedMessage
                      id="component.popconfirm.confirm"
                      defaultMessage="Are you sure to delete this record?"
                    />
                  }
                  onConfirm={() => remove(record)}
                >
                  <span className="cursor-pointer" style={{ color: red.primary }}>
                    <FormattedMessage id="component.button.remove" defaultMessage="Remove" />
                  </span>
                </Popconfirm>
              </Space>
            );
          }
          return (
            <Space>
              <span
                className="cursor-pointer"
                style={{ color: blue.primary }}
                onClick={(e) => saveRow(e, record)}
              >
                <FormattedMessage id="component.button.save" defaultMessage="Save" />
              </span>
              <Divider type="vertical" />
              <span className="cursor-pointer" onClick={(e) => cancel(e, record.key)}>
                <FormattedMessage id="component.popconfirm.cancel" defaultMessage="Cancel" />
              </span>
            </Space>
          );
        }
        return (
          <Space>
            <span
              className="cursor-pointer"
              style={{ color: blue.primary }}
              onClick={(e) => toggleEditable(e, record.key)}
            >
              <FormattedMessage id="component.tooltip.edit" defaultMessage="Edit" />
            </span>
            <Divider type="vertical" />
            <Popconfirm
              title={
                <FormattedMessage
                  id="component.popconfirm.confirm"
                  defaultMessage="Are you sure to delete this record?"
                />
              }
              onConfirm={() => {
                dispatch({
                  type: 'configuration/deleteBank',
                  payload: {
                    _id: record._id,
                  },
                });
              }}
            >
              <span className="cursor-pointer" style={{ color: red.primary }}>
                <FormattedMessage id="component.tooltip.delete" defaultMessage="Delete" />
              </span>
            </Popconfirm>
          </Space>
        );
      },
    },
  ];

  return (
    <PageContainer>
      <Card
        title={<FormattedMessage id="pages.tax.list" defaultMessage="Taxes List" />}
        className="card"
        bordered={false}
      >
        <Table
          loading={processing}
          columns={columns}
          dataSource={data}
          pagination={false}
          rowClassName={(record) => (record.editable ? styles.editable : '')}
        />
        <Button
          style={{ width: '100%', marginTop: 16, marginBottom: 8 }}
          type="dashed"
          onClick={newMember}
        >
          <PlusOutlined />
          &nbsp;
          <FormattedMessage id="pages.tax.create" defaultMessage="Create tax" />
        </Button>
      </Card>
    </PageContainer>
  );
};

export default connect(({ configuration, loading }: ConnectState) => ({
  bankAccountRecords: configuration.bankAccountRecords,
  processing:
    loading.effects['configuration/fetchBank'] ||
    loading.effects['configuration/createBank'] ||
    loading.effects['configuration/updateBank'] ||
    loading.effects['configuration/deleteBank'],
}))(BankForm);
