import type { Effect, Reducer } from 'umi';
import { history } from 'umi';
import { SystemConstantsService } from '@/services/system-constant';
import { AccountNumberService } from '@/services/account-number';
import { TaxService } from '@/services/tax';
import { CurrencyService } from '@/services/currency';
import { CompanyTypeService } from '@/services/company-type';
import { BankService } from '@/services/bank';
import { uploadImage } from '@/services/files';

import { message } from 'antd';

export type ConfigurationModelState = {
  systemConstants?: API.SystemConstants;
  accountNumberRecords?: API.AccountNumberRecord[];
  currentAccountNumber?: API.AccountNumberRecord;
  taxRecords?: API.TaxRecord[];
  currencyRecords?: API.Currency[];
  companyTypeRecords?: API.CompanyType[];
  bankAccountRecords?: API.Bank[];
  isUpdatingAccountNumber?: boolean;
};
export type ConfigurationModelType = {
  namespace: 'configuration';
  state: ConfigurationModelState;
  effects: {
    fetchSystemConstants: Effect;
    updateSystemConstants: Effect;
    fetchAccountNumber: Effect;
    createAccountNumber: Effect;
    updateAccountNumber: Effect;
    deleteAccountNumber: Effect;
    fetchTaxes: Effect;
    createTax: Effect;
    updateTax: Effect;
    deleteTax: Effect;
    fetchCurrency: Effect;
    createCurrency: Effect;
    updateCurrency: Effect;
    deleteCurrency: Effect;
    fetchCompanyType: Effect;
    createCompanyType: Effect;
    updateCompanyType: Effect;
    deleteCompanyType: Effect;
    fetchBank: Effect;
    createBank: Effect;
    updateBank: Effect;
    deleteBank: Effect;
  };
  reducers: {
    save: Reducer<ConfigurationModelState>;
    clear: Reducer<ConfigurationModelState>;
    handleCurrentAccountNumber: Reducer<ConfigurationModelState>;
  };
};

const ConfigurationModel: ConfigurationModelType = {
  namespace: 'configuration',

  state: {
    isUpdatingAccountNumber: false,
    accountNumberRecords: [],
  },

  effects: {
    *fetchSystemConstants(_, { call, put }) {
      const response = yield call(SystemConstantsService.querySystemConstants);
      yield put({
        type: 'save',
        payload: {
          systemConstants: response ? response.data : {},
        },
      });
    },
    *updateSystemConstants({ payload }, { call, put }) {
      const response = yield call(SystemConstantsService.updateSystemConstants, payload);
      if (response.data) {
        // message.success(
        //   useIntl().formatMessage({
        //     id: 'pages.systemConstants.success',
        //     defaultMessage: 'Update system constant successfule',
        //   }),
        // );
        yield put({
          type: 'save',
          payload: {
            systemConstants: response ? response.data : {},
          },
        });
      }
    },
    *fetchAccountNumber(_, { call, put }) {
      const response = yield call(AccountNumberService.queryAccountNumber);

      if (response) {
        const accountNumberRecords = response.data as API.AccountNumberRecord[];
        accountNumberRecords.forEach((item, index) => {
          item.key = index + 1;
          item.accountNumbers.forEach((child) => {
            child.key = `${child._id}`;
            if (!child.parentNumber) {
              const filterAccountNumberChild = item.accountNumbers.filter(
                (record) => record.parentNumber === child.accountNumber,
              );
              if (filterAccountNumberChild.length > 0) {
                child.children = filterAccountNumberChild;
              }
            }
          });
          item.accountNumbers = item.accountNumbers.filter((record) => record.level === 1);
        });
        yield put({
          type: 'save',
          payload: {
            accountNumberRecords: response ? response.data : [],
          },
        });
      }
    },
    *createAccountNumber({ payload }, { call }) {
      const response = yield call(AccountNumberService.createAccountNumber, payload);
      if (response.success) {
        message.success('Create account number  successful');
        history.push('/config/account-number/list');
      }
    },
    *updateAccountNumber({ payload }, { call, put }) {
      const response = yield call(AccountNumberService.updateAccountNumber, payload);
      if (response.success) {
        message.success('Update account number  successful');

        yield put({
          type: 'configuration/fetchAccountNumber',
        });
      }
    },
    *deleteAccountNumber({ payload }, { call, put }) {
      const response = yield call(AccountNumberService.deleteAccountNumber, payload);
      if (response.success) {
        message.success('Delete account number  successful');

        yield put({
          type: 'configuration/fetchAccountNumber',
        });
      }
    },
    *fetchTaxes(_, { call, put }) {
      const response = yield call(TaxService.queryTaxes);
      if (response) {
        const taxRecords = response.data as API.TaxRecord[];
        taxRecords.map((item, index) => {
          return {
            ...item,
            key: index + 1,
          };
        });
        yield put({
          type: 'save',
          payload: {
            taxRecords,
          },
        });
      }
    },
    *createTax({ payload }, { call, put }) {
      const response = yield call(TaxService.createSettingTax, payload);
      if (response.success) {
        message.success('Create tax successful');
        yield put({
          type: 'configuration/fetchTaxes',
        });
      }
    },
    *updateTax({ payload }, { call, put }) {
      const response = yield call(TaxService.updateSettingTax, payload);
      if (response.success) {
        message.success('Update tax successful');

        yield put({
          type: 'configuration/fetchTaxes',
        });
      }
    },
    *deleteTax({ payload }, { call, put }) {
      const response = yield call(TaxService.deleteSettingTax, payload);
      if (response.success) {
        message.success('Delete tax successful');
        yield put({
          type: 'configuration/fetchTaxes',
        });
      }
    },
    *fetchCurrency(_, { call, put }) {
      const response = yield call(CurrencyService.queryCurrency);
      if (response.success) {
        const currencyRecords = response.data as API.Currency[];
        currencyRecords.forEach((item, index) => {
          item.key = index + 1;
        });
        yield put({
          type: 'save',
          payload: {
            currencyRecords,
          },
        });
      }
    },
    *createCurrency({ payload }, { call, put }) {
      const response = yield call(CurrencyService.createCurrency, payload);
      if (response.success) {
        message.success('Create currency successful');

        yield put({
          type: 'configuration/fetchCurrency',
        });
        yield put({
          type: 'configuration/notify',
        });
      }
    },
    *updateCurrency({ payload }, { call, put }) {
      const response = yield call(CurrencyService.updateCurrency, payload);
      if (response.success) {
        message.success('Update currency successful');
        yield put({
          type: 'configuration/fetchCurrency',
        });
      }
    },
    *deleteCurrency({ payload }, { call, put }) {
      const response = yield call(CurrencyService.deleteCurrency, payload);
      if (response.success) {
        message.success('Delete currency successful');
        yield put({
          type: 'configuration/fetchCurrency',
        });
      }
    },
    *fetchCompanyType({ payload }, { call, put }) {
      const response = yield call(CompanyTypeService.queryCompanyType, payload);
      if (response.success) {
        const companyTypeRecords = response.data as API.CompanyType[];
        companyTypeRecords.forEach((item, index) => {
          item.key = index + 1;
        });
        yield put({
          type: 'save',
          payload: {
            companyTypeRecords,
          },
        });
      }
    },
    *createCompanyType({ payload }, { call, put }) {
      const response = yield call(CompanyTypeService.createCompanyType, payload);
      if (response.success) {
        message.success('Create company type successful');

        yield put({
          type: 'configuration/fetchCompanyType',
          payload: {
            limit: 10,
            page: 0,
          },
        });
      }
    },
    *updateCompanyType({ payload }, { call, put }) {
      const response = yield call(CompanyTypeService.updateCompanyType, payload);
      if (response.success) {
        message.success('Update company type successful');
        yield put({
          type: 'configuration/fetchCompanyType',
          payload: {
            limit: 10,
            page: 0,
          },
        });
      }
    },
    *deleteCompanyType({ payload }, { call, put }) {
      const response = yield call(CompanyTypeService.deleteCompanyType, payload);
      if (response.success) {
        message.success('Delete company type successful');
        yield put({
          type: 'configuration/fetchCompanyType',
          payload: {
            limit: 10,
            page: 0,
          },
        });
      }
    },
    *fetchBank({ payload }, { call, put }) {
      const response = yield call(BankService.queryBank, payload);
      if (response.success) {
        const bankAccountRecords = response.data as API.Bank[];
        bankAccountRecords.forEach((item, index) => {
          item.key = index + 1;
        });
        yield put({
          type: 'save',
          payload: {
            bankAccountRecords,
          },
        });
      }
    },
    *createBank({ payload }, { call, put }) {
      const resUrl = yield call(uploadImage, payload.logo);
      if (resUrl.success) {
        const response = yield call(BankService.createSettingBank, {
          ...payload,
          logo: resUrl.data.url,
        });
        if (response.success) {
          message.success('Create bank successful');
          yield put({
            type: 'configuration/fetchBank',
            payload: {
              limit: 10,
              page: 0,
            },
          });
        }
        return;
      }
      message.error('Upload image fail');
    },
    *updateBank({ payload }, { call, put }) {
      const resUrl = yield call(uploadImage, payload.logo);
      if (resUrl.success) {
        const response = yield call(BankService.updateSettingBank, payload);
        if (response.success) {
          message.success('Update company type successful');
          yield put({
            type: 'configuration/fetchBank',
            payload: {
              limit: 10,
              page: 0,
            },
          });
        }
        return;
      }
      message.error('Upload image fail');
    },
    *deleteBank({ payload }, { call, put }) {
      const response = yield call(BankService.deleteSettingBank, payload);
      if (response.success) {
        message.success('Delete bank successful');
        yield put({
          type: 'configuration/fetchBank',
          payload: {
            limit: 10,
            page: 0,
          },
        });
      }
    },
  },

  reducers: {
    save(state, { payload }) {
      return {
        ...state,
        ...payload,
      };
    },
    clear(state) {
      return {
        ...state,
      };
    },
    handleCurrentAccountNumber(state, { payload }) {
      const {
        type,
        accountNumberRecord,
      }: { type: 'update' | 'default'; accountNumberRecord: API.AccountNumberRecord } = payload;
      switch (type) {
        case 'update':
          return {
            ...state,
            isUpdatingAccountNumber: true,
            currentAccountNumber: accountNumberRecord,
          };
        default:
          return {
            ...state,
            currentAccountNumber: accountNumberRecord,
          };
      }
    },
  },
};

export default ConfigurationModel;
