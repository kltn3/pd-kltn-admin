import { stringify } from 'querystring';
import type { Reducer, Effect } from 'umi';
import { history } from 'umi';

import { AuthService } from '@/services/auth';
import { setAuthority } from '@/utils/authority';
import { getPageQuery } from '@/utils/utils';
import { getLocalStorage } from '@/utils/localStorage';
import { message } from 'antd';

export type AuthStateType = {
  accessToken?: string | null;
  status?: 'ok' | 'error';
  type?: string;
  currentAuthority?: 'user' | 'guest' | 'admin';
};

export type AuthModelType = {
  namespace: string;
  state: AuthStateType;
  effects: {
    login: Effect;
    logout: Effect;
  };
  reducers: {
    changeLoginStatus: Reducer<AuthStateType>;
  };
};

const AuthModel: AuthModelType = {
  namespace: 'auth',

  state: {
    status: undefined,
    accessToken: getLocalStorage('pd-authority'),
  },

  effects: {
    *login({ payload }, { call, put }) {
      const response = yield call(AuthService.login, payload);
      // Login successfully
      if (response.success) {
        yield put({
          type: 'changeLoginStatus',
          payload: response,
        });
        const urlParams = new URL(window.location.href);
        const params = getPageQuery();
        let { redirect } = params as { redirect: string };
        if (redirect) {
          const redirectUrlParams = new URL(redirect);
          if (redirectUrlParams.origin === urlParams.origin) {
            redirect = redirect.substr(urlParams.origin.length);
            if (redirect.match(/^\/.*#/)) {
              redirect = redirect.substr(redirect.indexOf('#') + 1);
            }
          } else {
            window.location.href = '/';
            return;
          }
        }
        // console.log(redirect);
        history.push(redirect || '/');
        message.success('🎉 🎉 🎉  Đăng nhập thành công!');
      }
    },

    logout() {
      const { redirect } = getPageQuery();
      // Note: There may be security issues, please note
      if (window.location.pathname !== '/user/login' && !redirect) {
        history.replace({
          pathname: '/user/login',
          search: stringify({
            redirect: window.location.href,
          }),
        });
      }
    },
  },

  reducers: {
    changeLoginStatus(state, { payload }) {
      setAuthority(payload.data.accessToken);
      return {
        ...state,
        accessToken: payload.data.accessToken,
        status: payload.status,
        type: payload.type,
      };
    },
  },
};

export default AuthModel;
