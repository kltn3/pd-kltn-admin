import type { MenuDataItem, Settings as ProSettings } from '@ant-design/pro-layout';
import type { AuthStateType } from './auth';
import type { GlobalModelState } from './global';
import type { UserModelState } from './user';
import type { ConfigurationModelState } from './configuration';

export { GlobalModelState, UserModelState };

export type Loading = {
  global: boolean;
  effects: Record<string, boolean | undefined>;
  models: {
    global?: boolean;
    menu?: boolean;
    setting?: boolean;
    user?: boolean;
    auth?: boolean;
    auth?: boolean;
    configuration?: boolean;
  };
};

export type ConnectState = {
  global: GlobalModelState;
  loading: Loading;
  settings: ProSettings;
  user: UserModelState;
  auth: AuthStateType;
  configuration: ConfigurationModelState;
};

export type Route = {
  routes?: Route[];
} & MenuDataItem;
