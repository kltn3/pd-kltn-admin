import type { Effect, Reducer } from 'umi';
import { AccountNumberService } from '@/services/account-number';
// import { message } from 'antd';

export type AccountNumberModelState = {
  systemConstants?: API.SystemConstants;
};

export type AccountNumberModelType = {
  namespace: 'account-number',
  state: AccountNumberModelState;
  effects: {
    fetchAccountNumber: Effect;
    createAccountNumber: Effect;
    updateAccountNumber: Effect;
    deleteAccountNumber: Effect;
  };
  reducers: {
    save: Reducer<AccountNumberModelState>;
    clear: Reducer<AccountNumberModelState>;
  };
};

const AccountNumberModel: AccountNumberModelType = {
  namespace: 'account-number',

  state: {},

  effects: {
    *fetchAccountNumber(_, { call, put }) {
      const response = yield call(AccountNumberService.queryAccountNumber);
      yield put({
        type: 'save',
        payload: {
          systemConstants: response ? response.data : {},
        },
      });
    },
    *createAccountNumber({ payload }, { call, put }) {
      const response = yield call(AccountNumberService.createAccountNumber, payload);
      console.log(response);
      // yield put({
      //   type: 'save',
      //   payload: {
      //     systemConstants: response ? response.data : {},
      //   },
      // });
    },
    *updateAccountNumber({ payload }, { call, put }) {
      const response = yield call(AccountNumberService.updateAccountNumber, payload);
      console.log(response);
      // yield put({
      //   type: 'save',
      //   payload: {
      //     systemConstants: response ? response.data : {},
      //   },
      // });
    },
    *deleteAccountNumber({ payload }, { call, put }) {
      const response = yield call(AccountNumberService.deleteAccountNumber, payload);
      console.log(response);
      // yield put({
      //   type: 'save',
      //   payload: {
      //     systemConstants: response ? response.data : {},
      //   },
      // });
    },
  },

  reducers: {
    save(state, { payload }) {
      return {
        ...state,
        ...payload,
      };
    },
    clear(state) {
      return {
        ...state,
      };
    },
  },
};

export default AccountNumberModel;
