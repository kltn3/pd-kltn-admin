import type { Effect, Reducer } from 'umi';

import { queryProfile } from '@/services/user';

export type Permission = {
  permissionKey: string;
  _id: string;
};

export type CurrentUser = {
  phone?: string;
  username?: string;
  status?: string;
  roleId?: string;
  roleInfo?: {
    status: string;
    _id: string;
    roleName: string;
    rolePermissions: Permission[];
  };
  _id?: string;
};

export type UserModelState = {
  test?: string;
  currentUser?: CurrentUser;
};

export type UserModelType = {
  namespace: 'user';
  state: UserModelState;
  effects: {
    fetchProfile: Effect;
  };
  reducers: {
    saveCurrentUser: Reducer<UserModelState>;
    changeNotifyCount: Reducer<UserModelState>;
  };
};

const UserModel: UserModelType = {
  namespace: 'user',

  state: {
    test: '',
    currentUser: {},
  },

  effects: {
    *fetchProfile(_, { call, put }) {
      const response = yield call(queryProfile);
      yield put({
        type: 'saveCurrentUser',
        payload: response.data,
      });
    },
  },

  reducers: {
    saveCurrentUser(state, action) {
      return {
        ...state,
        currentUser: action.payload[0] || {},
      };
    },
    changeNotifyCount(
      state = {
        currentUser: {},
      },
      action,
    ) {
      return {
        ...state,
        currentUser: {
          ...state.currentUser,
          notifyCount: action.payload.totalCount,
          unreadCount: action.payload.unreadCount,
        },
      };
    },
  },
};

export default UserModel;
